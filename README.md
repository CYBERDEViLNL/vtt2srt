# vtt2srt
very simple vtt to srt convertion

```
python vtt2srt.py input.vtt output.srt
```

or do a batch
```
find . -iname "*.vtt" -exec python vtt2srt.py {} {}.srt \;
```
